[TOC]
# Markdown

Texto normal... párrafos... En un lugar 
de la mancha de cuyo nombre no 
quiero acordarme.

Aqui empieza un segundo párrafo,
que tiene una linea de textos
aunque la escriba yo en muchas lineas.  
Esta linea se muestre en el mismo párrafo, pero que se respete el salto de linea.
Eso lo puedo conseguir con 2 espacios en blanco en la linea anterior... auqneu esta desaconsejado.\
Lo mejor es acabar la linea anterior con una contrabarra... hace lo mismo y es un caracter visible.

Los textos en los párraqfos los puedo formatear, en *cursiva de la buena*,
también en **negrita**. ***Negrita y cursiva***

Podeis reemplazar los asteriscos por guiones bajos: _CURSIVA_, __NEGRITA__... pero esta desaconsejado.

Otros estilos:

~~tachado~~

superindice ^1^

subindices --123--

==remarcado==

`codigo`

Markdown NO ES UN ESTANDAR... Cada intérprete de Markdown admite sus pequeñas variaciones.

Títulos:

# Titulo de nivel 1 ################################################################

Otro título de nivel 1
======================


Otro título de nivel 1
=

## Títulos de nivel 2

### Títulos de nivel 3

#### Títulos de nivel 4

##### Títulos de nivel 5

Otro título de nivel 2
-----------

Listas no ordenadas:
- item 1
- item 2
  Esto sería del **mismo** item

  Y otro párrafo del iotem
- item 3:
  * item 1
  * item2
  * item3:
    + item 1
    + item2
    + item3

Listas ordenadas:

1. Item 1
2. Item 2
3. Item 3
   1) Item 1
   2) Item 2
   3) Item 3

## Tablas

| Columna 1   | Columna 2       | Columna 3 |
| ----------- | --------------: | :-------: |
| Item1       | pues eso        | mas       |
| Item 2      | otra cosa       | 33        |
| Item 3      | algo            | 44        |

## Enlaces

[Mensaje que se ve](http://google.es)

<http://google.es>

## Imágenes
[Enlaces](#markdown)

![](https://s1.eestatic.com/2016/12/16/social/memes-humor-redes_sociales_178744040_23538138_1706x960.jpg)

El color azul `#768987`

## Listas de tareas

- [ ] tarea pendiente
- [x] Tarea acabada


## Código de programacion

    function reverseString(string:String):String
    {
        var reversed:String = new String();
        for(var i:int = string.length -1; i >= 0; i--)
            reversed += string.charAt(i);
        return reversed;
    }

    function reverseStringCQAlternative(string:String):String
    {
        return string.split('').reverse().join('');
    }

```js
function reverseString(string:String):String
{
    var reversed:String = new String();
    for(var i:int = string.length -1; i >= 0; i--)
        reversed += string.charAt(i);
    return reversed;
}

function reverseStringCQAlternative(string:String):String
{
    return string.split('').reverse().join('');
}
```


```py
import unicodedata

def ureverse(ustring):
    'Reverse a string including unicode combining characters'
    groupedchars = []
    uchar = list(ustring)
    while uchar:
        if unicodedata.combining(uchar[0]) != 0:
            groupedchars[-1] += uchar.pop(0)
        else:
            groupedchars.append(uchar.pop(0))
    # Grouped reversal
    groupedchars = groupedchars[::-1]
 
    return ''.join(groupedchars)

def say_string(s):
    return ' '.join([s, '=', ' | '.join(unicodedata.name(ch, '') for ch in s)])

def say_rev(s):
    print(f"Input:              {say_string(s)}")
    print(f"Character reversed: {say_string(s[::-1])}")
    print(f"Unicode reversed:   {say_string(ureverse(s))}")
    print(f"Unicode reverse²:   {say_string(ureverse(ureverse(s)))}")
        
if __name__ == '__main__':
    ucode = ''.join(chr(int(n[2:], 16)) for n in 
                     'U+0041 U+030A U+0073 U+0074 U+0072 U+006F U+0308 U+006D'.split())
    say_rev(ucode)
```

```mermaid
timeline
    title History of Social Media Platform
    2002 : LinkedIn
    2004 : Facebook
         : Google
    2005 : Youtube
    2006 : Twitter

```

```mermaid
sequenceDiagram
    Alice->>John: Hello John, how are you?
    John-->>Alice: Great!
    Alice-)John: See you later!
```

```mermaid

mindmap
  root((mindmap))
    Origins
      Long history
      ::icon(fa fa-book)
      Popularisation
        British popular psychology author Tony Buzan
    Research
      On effectiveness<br/>and features
      On Automatic creation
        Uses
            Creative techniques
            Strategic planning
            Argument mapping
    Tools
      Pen and paper
      Mermaid

```